package demo;

import java.util.List;
import demo.Pyramide;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class PyramideTest {
    static Pyramide pyramide = new Pyramide();

    @DisplayName("Elle doit retourner une liste de longueur (taille)*2 -1")
    @ParameterizedTest
    @ValueSource(ints = {1,2,5,45})
    public void testBonneLongueur(int taille){
        List<String> pyramideList = pyramide.pyramide(taille);
        assertEquals(taille*2-1, pyramideList.size());
    }

    @ParameterizedTest
    @ValueSource(ints = {-1,-21,0})
    @DisplayName("Elle doit ")
    public void testExceptionSiZeroOuInferieur(int taille){
        assertThrows(PyramideBadArgumentException.class, () -> pyramide.pyramide(taille));
    }

    @ParameterizedTest
    @ValueSource(ints = {12,145,22})
    @DisplayName("dzd")
    public void testChaineDEtoiles(int taille){
        List<String> pyramideList = pyramide.pyramide(taille);
        for (String s:pyramideList){
            assertTrue(s.matches("^[*]+$"));
        }
    }
}